﻿using System;

namespace Ejercicios_array_y_clases
{
    /*9- Crear una clase que permita ingresar valores enteros por teclado y nos muestre la tabla de multiplicar 
    de dicho valor. Finalizar el programa al ingresar el -1.*/

    class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();
            Program_multiplicacion mutl = new Program_multiplicacion();
            mutl.ingresar_numero();
        }
    }
    class Program_multiplicacion
    {
        public void tabla_multiplicar(int multpl)
        {
            int i = 0, result;

            for (i = 1; i <= 12; i++)
            {
                result = i * multpl;
                Console.WriteLine(i + " * " + multpl + " = " + result);
            }
        }

        public void ingresar_numero()
        {
            int num;
            do
            {
                Console.Write("\n" + "Ingrese el numero de la tabla que desea: ");
                num = int.Parse(Console.ReadLine() + "\n");
                if (num > -1)
                {
                    tabla_multiplicar(num);
                }
            } while (num > -1);
        }
    }
}