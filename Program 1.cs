﻿using System;

namespace Ejercicios_array_y_clases
{
    /*1- Un programa que pida al usuario 4 números, los memorice (utilizando un array), 
    calcule su media aritmética y después muestre en pantalla la media y los datos tecleados.*/
    
    class Program
    {
        static void Main(string[] args)
        {
            
            int numero_contador = 0;
            int suma = 0;
            int media_arit = 0;
            int[] aNumeros = new int[4];
            
            Console.Clear();

            for (int i = 0; i < aNumeros.Length; i++)
            {
                numero_contador = numero_contador + 1;
                Console.Write("\nIntroduzca su numero {0}: ", numero_contador);
                aNumeros [i]= int.Parse(Console.ReadLine());
            } 
            
            foreach (int x in aNumeros) 
            {
                suma = suma + x;
                
            }

            media_arit = suma / 4;
            Console.WriteLine("\nLa media aritmetica de los numeros {0}, {1}, {2} y {3} es igual a: {4}\n",aNumeros[0],aNumeros[1],aNumeros[2],aNumeros[3], media_arit);
        }
    }
}