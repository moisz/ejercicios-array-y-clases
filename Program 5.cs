﻿using System;

namespace Ejercicios_array_y_clases
{
    class Program
    {
        /*5- Un programa que prepare espacio para un máximo de 100 nombres. El usuario deberá ir introduciendo un nombre cada 
        vez, hasta que se pulse Intro sin teclear nada, momento en el que dejarán de pedirse más nombres y se mostrará en pantalla 
        la lista de los nombres que se han introducido.*/
        
        static void Main(string[] args)
        {
            
            int contador = 0, contador2 = 0, i;
            String[] aNombres = new String[100];

            Console.WriteLine("\n");
            Console.Clear();

            do
            {
                Console.Write("Escriba el nombre que desea registrar: ");
                aNombres[contador] = Console.ReadLine();
                contador += 1;
            } while (aNombres[contador - 1] != "");

            Console.WriteLine("\n");
            Console.Clear();

            for (i = 0; i < contador - 1; i++)
            { 
                contador2 += 1;
                Console.Write("El nombre #{0} registrado es: ", contador2);
                Console.WriteLine(aNombres[i] + "\n");
            }
        }
    }
}