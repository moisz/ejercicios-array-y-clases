﻿using System;

namespace Ejercicios_array_y_clases
{
    
    /* 10-Confeccionar una clase que permita carga el nombre y la edad de una persona. 
    Mostrar los datos cargados. Imprimir un mensaje si es mayor de edad (edad>=18).*/
    
    class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();
            Datos_persona person = new Datos_persona();
            person.Registrar_dts_psn();
            person.mostrar_dts_psn();
            person.Comprobar_edad();            
        }
    }
    
    class Datos_persona
    {
        string nombre;
        int edad;

        public void Registrar_dts_psn()
        {
            Console.Write("Escriba tu nombre: ");
            nombre = Console.ReadLine();
            Console.Write("Escriba tu edad: ");
            edad = int.Parse(Console.ReadLine());
        }

        public void mostrar_dts_psn()
        {
            Console.Clear();
            Console.WriteLine("\n" + "Nombre: " + nombre);
            Console.WriteLine("Edad: " + edad);
        }

        public void Comprobar_edad()
        {
            if (edad >= 18)
            {
                Console.WriteLine("{0} es mayor de edad.", nombre);
            }
            else
            {
                Console.WriteLine("{0} no es mayor de edad.", nombre);
            }
            Console.ReadKey();
        }

    }
}