﻿using System;

namespace Ejercicios_array_y_clases
{
    /*2- Un programa que pida al usuario 5 números reales (pista: necesitarás un array de "float")
     y luego los muestre en el orden contrario al que se introdujeron.*/
    
    class Program
    {
        static void Main(string[] args)
        {
            
            int numero_contador = 0;
            float[] aNumeros = new float[5];
            
            Console.WriteLine("\n");
            Console.Clear();

            for (int i = 0; i < aNumeros.Length; i++)
            {
                numero_contador = numero_contador + 1;
                Console.Write("Introduzca su numero {0}: ", numero_contador);
                aNumeros [i]= float.Parse(Console.ReadLine());
            }

            Console.WriteLine("\n---Numeros en el orden contrario que se introducieron---\n");

            Array.Reverse(aNumeros);
            
            foreach (int x in aNumeros) 
            {
                Console.Write("Numero: ");
                Console.WriteLine(x);
            }
        }
    }
}